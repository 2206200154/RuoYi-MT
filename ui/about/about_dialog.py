# -*- coding: utf-8 -*-

###########################################################################
# Python code generated with wxFormBuilder (version 3.10.1-0-g8feb16b)
# http://www.wxformbuilder.org/
##
# PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.adv

###########################################################################
# Class AboutDialog
###########################################################################


class AboutDialog (wx.Dialog):

    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"关于我们", pos=wx.DefaultPosition, size=wx.Size(
            330, 500), style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        self.SetBackgroundColour(wx.Colour(255, 255, 255))

        bSizer = wx.BoxSizer(wx.VERTICAL)

        bSizer2 = wx.BoxSizer(wx.VERTICAL)

        self.m_static_text_version = wx.StaticText(
            self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_static_text_version.Wrap(-1)

        bSizer2.Add(self.m_static_text_version, 0, wx.ALL | wx.EXPAND, 5)

        self.m_scrolled_window = wx.ScrolledWindow(
            self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.VSCROLL)
        self.m_scrolled_window.SetScrollRate(0, 5)
        bSizer17 = wx.BoxSizer(wx.VERTICAL)

        self.m_staticText21 = wx.StaticText(
            self.m_scrolled_window, wx.ID_ANY, u"[介绍]", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText21.Wrap(-1)

        bSizer17.Add(self.m_staticText21, 0, wx.ALL, 5)

        self.m_staticText22 = wx.StaticText(
            self.m_scrolled_window, wx.ID_ANY, u"本软件是为了方便大家修改若依框架的包名、项目\n名等制作的，如同若依框架一样免费使用，功能如\n下：", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText22.Wrap(-1)

        bSizer17.Add(self.m_staticText22, 0, wx.ALL, 5)

        self.m_staticText23 = wx.StaticText(
            self.m_scrolled_window, wx.ID_ANY, u"1、修改包名、目录名、项目名等", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText23.Wrap(-1)

        bSizer17.Add(self.m_staticText23, 0, wx.ALL, 5)

        self.m_staticText24 = wx.StaticText(
            self.m_scrolled_window, wx.ID_ANY, u"2、修改前可以配置其他的修改参数，例如配置数据\n库地址、名称、账号、密码等，在修改时会将配置文\n件中的对应参数一起修改掉", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText24.Wrap(-1)

        bSizer17.Add(self.m_staticText24, 0, wx.ALL, 5)

        self.m_staticText25 = wx.StaticText(
            self.m_scrolled_window, wx.ID_ANY, u"3、未来可能会加入更多的一键操作功能，敬请期待", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText25.Wrap(-1)

        bSizer17.Add(self.m_staticText25, 0, wx.ALL, 5)

        self.m_staticText27 = wx.StaticText(
            self.m_scrolled_window, wx.ID_ANY, u"[说明]", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText27.Wrap(-1)

        bSizer17.Add(self.m_staticText27, 0, wx.ALL, 5)

        self.m_staticText28 = wx.StaticText(
            self.m_scrolled_window, wx.ID_ANY, u"1、在使用过程中如果遇到问题，请联系作者进行反\n馈", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText28.Wrap(-1)

        bSizer17.Add(self.m_staticText28, 0, wx.ALL, 5)

        self.m_staticText29 = wx.StaticText(
            self.m_scrolled_window, wx.ID_ANY, u"2、本软件只有一个地方是网络请求，就是检测更新\n，检测更新是爬取的gitee页面来实现的，可以放心\n使用", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText29.Wrap(-1)

        bSizer17.Add(self.m_staticText29, 0, wx.ALL, 5)

        self.m_scrolled_window.SetSizer(bSizer17)
        self.m_scrolled_window.Layout()
        bSizer17.Fit(self.m_scrolled_window)
        bSizer2.Add(self.m_scrolled_window, 1, wx.EXPAND, 5)

        bSizer4 = wx.BoxSizer(wx.HORIZONTAL)

        self.m_hyperlink1 = wx.adv.HyperlinkCtrl(
            self, wx.ID_ANY, u"软件官网", u"https://gitee.com/lpf_project/common-tools", wx.DefaultPosition, wx.Size(-1, -1), wx.adv.HL_DEFAULT_STYLE)
        self.m_hyperlink1.SetMinSize(wx.Size(-1, 30))

        bSizer4.Add(self.m_hyperlink1, 0, wx.ALL, 5)

        self.m_hyperlink2 = wx.adv.HyperlinkCtrl(
            self, wx.ID_ANY, u"若依官网", u"http://ruoyi.vip/", wx.DefaultPosition, wx.Size(-1, -1), wx.adv.HL_DEFAULT_STYLE)
        self.m_hyperlink2.SetMinSize(wx.Size(-1, 30))

        bSizer4.Add(self.m_hyperlink2, 0, wx.ALL, 5)

        bSizer2.Add(bSizer4, 0, wx.EXPAND, 5)

        bSizer.Add(bSizer2, 1, wx.ALL | wx.EXPAND, 5)

        bSizer3 = wx.BoxSizer(wx.VERTICAL)

        self.m_button_close = wx.Button(
            self, wx.ID_CANCEL, u"关闭", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer3.Add(self.m_button_close, 0, wx.ALL, 5)

        bSizer.Add(bSizer3, 0, wx.ALIGN_RIGHT | wx.ALL, 5)

        self.SetSizer(bSizer)
        self.Layout()

        self.Centre(wx.BOTH)

    def __del__(self):
        pass
