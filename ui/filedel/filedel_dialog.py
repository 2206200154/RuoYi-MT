# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version 3.10.1-0-g8feb16b3)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class FileDelDialog
###########################################################################

class FileDelDialog ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"批量删除文件", pos = wx.DefaultPosition, size = wx.Size( 500,500 ), style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		b_sizer_root = wx.BoxSizer( wx.VERTICAL )

		sb_sizer_dir_picker = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"选择根目录" ), wx.VERTICAL )

		self.m_dirPicker = wx.DirPickerCtrl( sb_sizer_dir_picker.GetStaticBox(), wx.ID_ANY, wx.EmptyString, u"Select a folder", wx.DefaultPosition, wx.DefaultSize, wx.DIRP_DEFAULT_STYLE )
		sb_sizer_dir_picker.Add( self.m_dirPicker, 0, wx.ALL|wx.EXPAND, 5 )


		b_sizer_root.Add( sb_sizer_dir_picker, 0, wx.ALL|wx.EXPAND, 5 )

		sb_sizer_select_file_or_dir = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"要删除的文件或文件夹" ), wx.VERTICAL )

		b_sizer_select_file_or_dir = wx.BoxSizer( wx.HORIZONTAL )

		b_sizer_select_file_or_dir_column1 = wx.BoxSizer( wx.VERTICAL )

		self.m_check_box_dot_settings = wx.CheckBox( sb_sizer_select_file_or_dir.GetStaticBox(), wx.ID_ANY, u".settings", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_check_box_dot_settings.SetValue(True)
		b_sizer_select_file_or_dir_column1.Add( self.m_check_box_dot_settings, 0, wx.ALL, 5 )

		self.m_check_box_dir_target = wx.CheckBox( sb_sizer_select_file_or_dir.GetStaticBox(), wx.ID_ANY, u"target", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_check_box_dir_target.SetValue(True)
		b_sizer_select_file_or_dir_column1.Add( self.m_check_box_dir_target, 0, wx.ALL, 5 )


		b_sizer_select_file_or_dir.Add( b_sizer_select_file_or_dir_column1, 0, wx.SHAPED, 5 )

		b_sizer_select_file_or_dir_column2 = wx.BoxSizer( wx.VERTICAL )

		self.m_check_box_dot_classpath = wx.CheckBox( sb_sizer_select_file_or_dir.GetStaticBox(), wx.ID_ANY, u".classpath", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_check_box_dot_classpath.SetValue(True)
		b_sizer_select_file_or_dir_column2.Add( self.m_check_box_dot_classpath, 0, wx.ALL, 5 )

		self.m_check_box_dot_project = wx.CheckBox( sb_sizer_select_file_or_dir.GetStaticBox(), wx.ID_ANY, u".project", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_check_box_dot_project.SetValue(True)
		b_sizer_select_file_or_dir_column2.Add( self.m_check_box_dot_project, 0, wx.ALL, 5 )


		b_sizer_select_file_or_dir.Add( b_sizer_select_file_or_dir_column2, 0, wx.EXPAND, 5 )

		b_sizer_select_file_or_dir_column3 = wx.BoxSizer( wx.VERTICAL )

		self.m_check_box_dot_idea = wx.CheckBox( sb_sizer_select_file_or_dir.GetStaticBox(), wx.ID_ANY, u".idea", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_check_box_dot_idea.SetValue(True)
		b_sizer_select_file_or_dir_column3.Add( self.m_check_box_dot_idea, 0, wx.ALL, 5 )

		self.m_check_box_dot_iml = wx.CheckBox( sb_sizer_select_file_or_dir.GetStaticBox(), wx.ID_ANY, u".iml", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_check_box_dot_iml.SetValue(True)
		b_sizer_select_file_or_dir_column3.Add( self.m_check_box_dot_iml, 0, wx.ALL, 5 )


		b_sizer_select_file_or_dir.Add( b_sizer_select_file_or_dir_column3, 0, wx.SHAPED, 5 )

		b_sizer_select_file_or_dir_column4 = wx.BoxSizer( wx.VERTICAL )

		self.m_check_box_mac_dot_ = wx.CheckBox( sb_sizer_select_file_or_dir.GetStaticBox(), wx.ID_ANY, u"._", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_check_box_mac_dot_.SetToolTip( u"Mac电脑生成的._开头的文件" )

		b_sizer_select_file_or_dir_column4.Add( self.m_check_box_mac_dot_, 0, wx.ALL, 5 )

		self.m_check_box_mac_dot_DS_Store = wx.CheckBox( sb_sizer_select_file_or_dir.GetStaticBox(), wx.ID_ANY, u".DS_Store", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_check_box_mac_dot_DS_Store.SetToolTip( u"Mac电脑生成的.DS_Store文件" )

		b_sizer_select_file_or_dir_column4.Add( self.m_check_box_mac_dot_DS_Store, 0, wx.ALL, 5 )


		b_sizer_select_file_or_dir.Add( b_sizer_select_file_or_dir_column4, 0, wx.SHAPED, 5 )

		b_sizer_select_file_or_dir_column5 = wx.BoxSizer( wx.VERTICAL )

		self.m_check_box_mac_MACOSX = wx.CheckBox( sb_sizer_select_file_or_dir.GetStaticBox(), wx.ID_ANY, u"__MACOSX", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_check_box_mac_MACOSX.SetToolTip( u"Mac电脑生成的__MACOSX文件夹" )

		b_sizer_select_file_or_dir_column5.Add( self.m_check_box_mac_MACOSX, 0, wx.ALL, 5 )

		self.m_check_box_node_modules = wx.CheckBox( sb_sizer_select_file_or_dir.GetStaticBox(), wx.ID_ANY, u"node_modules", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_check_box_node_modules.SetToolTip( u"前端node产生的编译文件" )

		b_sizer_select_file_or_dir_column5.Add( self.m_check_box_node_modules, 0, wx.ALL, 5 )


		b_sizer_select_file_or_dir.Add( b_sizer_select_file_or_dir_column5, 0, wx.SHAPED, 5 )


		sb_sizer_select_file_or_dir.Add( b_sizer_select_file_or_dir, 1, wx.EXPAND, 5 )

		self.m_static_text_input_tip = wx.StaticText( sb_sizer_select_file_or_dir.GetStaticBox(), wx.ID_ANY, u"其他后缀、文件名、文件夹名请在下面输入，多个以分号（;）隔开", wx.DefaultPosition, wx.Size( -1,-1 ), 0 )
		self.m_static_text_input_tip.Wrap( -1 )

		sb_sizer_select_file_or_dir.Add( self.m_static_text_input_tip, 0, wx.ALL, 5 )

		b_sizer_input_file_or_dir = wx.BoxSizer( wx.VERTICAL )

		self.m_text_ctrl_input_file_or_dir = wx.TextCtrl( sb_sizer_select_file_or_dir.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( -1,50 ), wx.TE_MULTILINE )
		b_sizer_input_file_or_dir.Add( self.m_text_ctrl_input_file_or_dir, 1, wx.ALL|wx.EXPAND, 5 )


		sb_sizer_select_file_or_dir.Add( b_sizer_input_file_or_dir, 0, wx.EXPAND, 5 )


		b_sizer_root.Add( sb_sizer_select_file_or_dir, 0, wx.ALL|wx.EXPAND, 5 )

		b_sizer_btns = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button_delete = wx.Button( self, wx.ID_ANY, u"删除", wx.DefaultPosition, wx.DefaultSize, 0 )
		b_sizer_btns.Add( self.m_button_delete, 0, wx.ALL, 5 )

		self.m_button_reset = wx.Button( self, wx.ID_ANY, u"重置", wx.DefaultPosition, wx.DefaultSize, 0 )
		b_sizer_btns.Add( self.m_button_reset, 0, wx.ALL, 5 )

		self.m_gauge = wx.Gauge( self, wx.ID_ANY, 100, wx.DefaultPosition, wx.DefaultSize, wx.GA_HORIZONTAL )
		self.m_gauge.SetValue( 0 )
		b_sizer_btns.Add( self.m_gauge, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )


		b_sizer_root.Add( b_sizer_btns, 0, wx.EXPAND, 5 )

		sb_sizer_log = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"删除记录" ), wx.VERTICAL )

		self.m_text_log = wx.TextCtrl( sb_sizer_log.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_READONLY )
		sb_sizer_log.Add( self.m_text_log, 1, wx.ALL|wx.EXPAND, 5 )


		b_sizer_root.Add( sb_sizer_log, 1, wx.ALL|wx.EXPAND, 5 )


		self.SetSizer( b_sizer_root )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_button_delete.Bind( wx.EVT_BUTTON, self.OnClickEventDelete )
		self.m_button_reset.Bind( wx.EVT_BUTTON, self.OnClickEventReset )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def OnClickEventDelete( self, event ):
		event.Skip()

	def OnClickEventReset( self, event ):
		event.Skip()


