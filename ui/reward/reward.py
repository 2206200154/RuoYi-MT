# -*- coding: utf-8 -*-
# @Time : 2021/02/23
# @Author : ricky
# @File : reward.py
# @Software: vscode
"""
打赏页面
"""
import wx
from . import reward_dialog as dialog
from utils import path


class Reward(dialog.RewardDialog):
    def __init__(self, parent):
        dialog.RewardDialog.__init__(self, parent)
        image_file = path.resource_path('img/wx_reward.png')
        image = wx.Image(image_file, wx.BITMAP_TYPE_ANY)
        image.Rescale(512, 300, wx.IMAGE_QUALITY_HIGH)
        bitmap = image.ConvertToBitmap()
        self.m_bitmap_wx.SetBitmap(bitmap)
        self.Centre()

    def OnOk(self, event):
        self.EndModal(wx.ID_OK)
        wx.MessageDialog(None, '谢谢您的打赏！保佑您的代码永无BUG！', '亲爱的帅哥美女大老板：',
                         wx.OK).ShowModal()

    def OnCancel(self, event):
        self.EndModal(wx.ID_CANCEL)
        wx.MessageDialog(None, '加油努力挣大钱！全面小康，就靠您赏！', '亲爱的帅哥美女大老板：',
                         wx.OK).ShowModal()
